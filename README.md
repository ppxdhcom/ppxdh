# 皮皮虾导航

## 请Ctrl+D收藏本网页地址，确保永久访问

### 最新网址：[http://ppx1.xyz](http://ppx1.xyz)

### 发布页①：[https://ppxdhwz.xyz/](https://ppxdhwz.xyz/)（点开之后，收藏）
### 发布页②：[https://bienao.gitbook.io/ppxdh/](https://bienao.gitbook.io/ppxdh/)（点开之后，收藏）

### 邮箱：057500@gmail.com（收藏好邮箱，发任意消息会自动回复最新网址）

<br/><br/>

> 网站网址打不开？<br/><br/>
> 1、国内聊天软件（微信、QQ）不能直接打开。解决方法：复制到浏览器打开
> 
> 2、国内部分浏览器屏蔽
> 解决方法：使用防屏蔽浏览器。推荐：[chrome浏览器](https://www.google.cn/chrome/)、[X浏览器](https://www.xbext.com/)、[VIA浏览器](https://viayoo.com/zh-cn/)、[微软Edge](https://www.microsoft.com/zh-cn/edge)、[火狐](http://www.firefox.com.cn/)、[Yandex](https://browser.yandex.com/)、[kiwibrowser](https://kiwibrowser.com/)
> 
> 3、移动宽带/网络部分网站打不开。方法：建议使用电信。
> 
> 4、还是打不开。方法：使用备用网址，或者挂梯子


